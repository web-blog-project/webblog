package de.awacademy.webblogproject.comment;

import de.awacademy.webblogproject.article.Article;
import de.awacademy.webblogproject.article.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommentService {

    private CommentRepository commentRepository;
    private ArticleRepository articleRepository;

    @Autowired
    public CommentService(CommentRepository commentRepository, ArticleRepository articleRepository) {
        this.commentRepository = commentRepository;
        this.articleRepository = articleRepository;
    }

    public List<Comment> getCommentList() {
        return commentRepository.findAllByOrderByPostedAtDesc();
    }

    public void add(Comment comment, Integer articleId) {
        Optional<Article> article = articleRepository.findById(articleId);
        comment.setArticle(article.get());
        commentRepository.save(comment);
    }

//    public Comment getComment(Integer commentId) {
//        Optional<Comment> comment = commentRepository.findById(commentId);
//        return comment.get();
//    }
}
