package de.awacademy.webblogproject.comment;

import de.awacademy.webblogproject.article.ArticleService;
import de.awacademy.webblogproject.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.Instant;

@Controller
public class CommentController {

    private CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping("/{articleId}/comment/")
    public String index(Model model, @PathVariable Integer articleId) {
        model.addAttribute("commentList", commentService.getCommentList());
        model.addAttribute("commentDTO", new CommentDTO(""));
        return "comment";
    }

    @GetMapping("/{articleId}/comment/add")
    public String create(Model model, @PathVariable Integer articleId) {
        model.addAttribute("comment", new Comment());
        model.addAttribute("articleId", articleId);
        return "comment";
    }

    @PostMapping("/{articleId}/comment/submit")
    public String submit(@ModelAttribute(value = "comment") CommentDTO commentDTO, BindingResult bindingResult,
                         @PathVariable Integer articleId, @ModelAttribute("sessionUser") User sessionUser){
        if(commentDTO.getText().isEmpty()){
            bindingResult.addError(new FieldError("comment", "text", "Bitte mindestens ein Zeichen eingeben"));
        }
        if (bindingResult.hasErrors()) {
            return "comment";
        }
        Comment comment = new Comment(sessionUser, commentDTO.getText(), Instant.now());
        commentService.add(comment, articleId);
        return "redirect:/";
    }
}