package de.awacademy.webblogproject.comment;

import javax.validation.constraints.NotEmpty;

public class CommentDTO {

    @NotEmpty
    private String text;

    public CommentDTO(@NotEmpty String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
