package de.awacademy.webblogproject.comment;

import de.awacademy.webblogproject.article.Article;
import de.awacademy.webblogproject.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.Instant;

@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private Article article;
    @ManyToOne
    private User user;

    private Instant postedAt;

    private String text;

    public Comment() {
    }

    public Comment(User user, String text, Instant postedAt) {
        this.user = user;
        this.text = text;
        this.postedAt = postedAt;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public String getText() {
        return text;
    }

    public User getUser() {
        return user;
    }

    public Integer getId() {
        return id;
    }

    public Instant getPostedAt() {
        return postedAt;
    }
}


