package de.awacademy.webblogproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebBlogProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebBlogProjectApplication.class, args);
	}

}
