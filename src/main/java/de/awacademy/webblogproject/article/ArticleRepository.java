package de.awacademy.webblogproject.article;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/*
CRUD : Create, Read, Update, Delete
Implementierung des Interfaces übernimmt Spring.
Ergebnis: eine Bean mit dem Namen articleRepository
 */

public interface ArticleRepository extends CrudRepository<Article, Integer> {

    List<Article>findAll();
    // Artikel in chronologischer Reihenfolge
    List<Article> findAllByOrderByIdDesc();

//    Optional<Article> findById(Integer id);
}
