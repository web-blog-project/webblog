package de.awacademy.webblogproject.article;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ArticleService {

    private ArticleRepository articleRepository;

    @Autowired
    public ArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public List<Article> getArticleList() {
        // Artikel in chronologischer Reihenfolge
        return articleRepository.findAllByOrderByIdDesc();
    }
}
