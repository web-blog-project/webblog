package de.awacademy.webblogproject.article;

import de.awacademy.webblogproject.comment.Comment;

import javax.persistence.*;
import java.util.List;

@Entity
public class Article {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String title;
    private String text;

    @OneToMany(mappedBy = "article")
    private List<Comment> comments;

    public Article(String title, String text) {
        this.title = title;
        this.text = text;
    }

    public Article() {

    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public List<Comment> getComments() {
        return comments;
    }
}
