package de.awacademy.webblogproject.user;

import de.awacademy.webblogproject.comment.Comment;

import javax.persistence.*;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    private String username;
    private String password;

    @OneToMany(mappedBy = "user")
    private List<Comment> comments;

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

//    public long getId() {
//        return id;
//    }
//
//    public List<Comment> getComments() {
//        return comments;
//    }


}
